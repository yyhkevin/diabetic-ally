# Diabetic Ally

Refer to Figma prototype [here](https://www.figma.com/proto/cKllxmT5kiu06DK92xuqec/HealthHack?type=design&node-id=4-29&t=1MrdkBp7x0ODK9YY-1&scaling=scale-down&page-id=0%3A1&starting-point-node-id=4%3A29&mode=design).

Diabetic Ally aims to streamline diabetes management by offering essential features:

1. Summary of Daily Activity: Personalized programs for medication reminders, meal recipes, and exercise recommendations in one place.
2. Medication Information and Advice: Simplifies access to medication details, tracks schedules, and sends reminders to avoid missed doses.
3. Dietary Management: Tailored meal plans and easy recipes ensure optimal sugar levels and overall health.
4. Learn and Share: Access to articles, videos, and a supportive community for sharing experiences and seeking advice.
5. Emergency Contact: Alerts designated contacts and shares live location during dangerous sugar level episodes.

The project is useful as it addresses complex diabetes management challenges comprehensively, simplifying medication, promoting healthy habits, providing educational resources, fostering community support, and ensuring safety during emergencies. Diabetes Ally empowers individuals to manage their condition effectively and enhance overall well-being with confidence.

## Testing the app
A precompiled Android APK has been included at [diabetic_ally.apk](diabetic_ally.apk) for your convenience.

If you wish to build the app from source, refer to the next section.

## Building the app from source (both Android and iOS)
1. Ensure Flutter is installed
2. Connect a physical device / use an emulator
3. Run `flutter run`

## Important notes
Flutter is chosen as it is a code once run everywhere platform. It is also chosen for performance purposes.

The project is currently under further construction.
A workable app mockup that showcases what is to be expected is available by compiling this Flutter project and installing on the tester's device.

However, more work is to be done to complete the remaining screens and widgets.
Much of the content is hardcoded - posts, username, articles etc.

For getting to market in the soonest possible time, Firebase is proposed as the backend for user account management, notification and data.

## Screenshots
![Home](screenshots/flutter_01.png)
![Community](screenshots/flutter_02.png)
![Learn](screenshots/flutter_03.png)
![Medicine](screenshots/flutter_04.png)