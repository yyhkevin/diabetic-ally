import 'package:diabetic_ally/core/theme_colors.dart';
import 'package:diabetic_ally/features/community/models/community_post_model.dart';
import 'package:diabetic_ally/features/community/widgets/community_post_widget.dart';
import 'package:flutter/material.dart';

class CommunityPage extends StatelessWidget {
  const CommunityPage({Key? key}) : super(key: key);

  static List<CommunityPostModel> posts = [
    CommunityPostModel(
      username: "Anonymous Deer",
      postedAt: DateTime.now().subtract(const Duration(hours: 3)),
      title: "How do I support my friend?",
      postContent:
          "So my friend had just been diagnosed with diabetes (type 1). I understand that it is not very common and I would like to hear what the community has to say...",
      likesCount: 15,
      commentsCount: 3,
    ),
    CommunityPostModel(
      username: "Tan Wei Jie",
      postedAt: DateTime.now().subtract(const Duration(hours: 5)),
      title: "Just sharing something to lighten the mood!",
      postContent:
          "I found this article which talks about how doing sports can improve our mood. Do check it out and support the author~",
      imageUri: "assets/images/sports.jpg",
      likesCount: 22,
      commentsCount: 1,
    ),
  ];

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Stack(
        children: [
          ListView.separated(
            padding: const EdgeInsets.all(21),
            itemCount: posts.length,
            itemBuilder: (context, index) =>
                CommunityPostWidget(post: posts[index]),
            separatorBuilder: (context, index) =>
                const Divider(height: 25, thickness: 0.5),
          ),
          Positioned(
            right: 20,
            bottom: 20,
            child: FloatingActionButton.small(
                backgroundColor: const Color(ThemeColors.prime),
                onPressed: () {
                  // TODO create post page
                },
                child: const Icon(Icons.add, color: Colors.white)),
          ),
        ],
      ),
    );
  }
}
