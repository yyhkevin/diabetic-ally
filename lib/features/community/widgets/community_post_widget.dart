import 'package:diabetic_ally/features/community/models/community_post_model.dart';
import 'package:flutter/material.dart';

class CommunityPostWidget extends StatelessWidget {
  final CommunityPostModel post;

  const CommunityPostWidget({Key? key, required this.post}) : super(key: key);

  String get postedAt {
    final durationAgo = DateTime.now().difference(post.postedAt);

    if (durationAgo.inMinutes < 60) {
      return "${durationAgo.inMinutes} minutes ago";
    } else if (durationAgo.inHours < 24) {
      return "${durationAgo.inHours} hours ago";
    } else if (durationAgo.inDays < 7) {
      return "${durationAgo.inDays} days ago";
    }
    return "${post.postedAt}"; // TODO
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          children: [
            const Icon(Icons.account_circle, size: 36),
            const SizedBox(width: 5),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  post.username,
                  style: const TextStyle(fontWeight: FontWeight.bold),
                ),
                Text(postedAt),
              ],
            ),
          ],
        ),
        const SizedBox(height: 12),
        Text(
          post.title,
          style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
        ),
        if (post.postContent != null) ...[
          const SizedBox(height: 8),
          Text(
            post.postContent!,
            style: const TextStyle(fontSize: 15),
          ),
        ],
        if (post.imageUri != null) ...[
          const SizedBox(height: 8),
          ClipRRect(
              borderRadius: BorderRadius.circular(8),
              child: Image.asset(post.imageUri!)),
        ],
        const SizedBox(height: 10),
        Row(
          children: [
            Text("👍 ${post.likesCount}"),
            const SizedBox(width: 12),
            Text("💬 ${post.commentsCount}"),
          ],
        ),
      ],
    );
  }
}
