class CommunityPostModel {
  final String username;
  final DateTime postedAt;
  final String title;
  final String? postContent;
  final String? imageUri;
  final int likesCount;
  final int commentsCount;

  const CommunityPostModel({
    required this.username,
    required this.postedAt,
    required this.title,
    this.postContent,
    this.imageUri,
    required this.likesCount,
    required this.commentsCount,
  });
}
