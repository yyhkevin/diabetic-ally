import 'package:diabetic_ally/core/theme_colors.dart';
import 'package:diabetic_ally/features/log_meal/log_camera_page.dart';
import 'package:diabetic_ally/features/log_meal/widgets/input_text_field.dart';
import 'package:diabetic_ally/features/log_meal/widgets/submit_button.dart';
import 'package:flutter/material.dart';

class LogMealPage extends StatelessWidget {
  const LogMealPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Expanded(
                child: ListView(
              children: [
                const Text("Log a Meal",
                    style:
                        TextStyle(fontWeight: FontWeight.bold, fontSize: 18)),
                const SizedBox(height: 10),
                GestureDetector(
                  onTap: () {
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) => const LogCameraPage()));
                  },
                  child: Stack(
                    alignment: Alignment.center,
                    children: [
                      ClipRRect(
                        borderRadius: BorderRadius.circular(8),
                        child: ColorFiltered(
                          colorFilter: ColorFilter.mode(
                            Colors.white.withOpacity(0.3),
                            BlendMode.modulate,
                          ),
                          child: Image.asset("assets/images/foods.jpg",
                              height: 150, width: 350, fit: BoxFit.cover),
                        ),
                      ),
                      const Text("📷 Add picture",
                          style: TextStyle(fontSize: 18)),
                    ],
                  ),
                ),
                const Text(
                    "Add a picture to automatically generate the fields below."),
                const SizedBox(height: 10),
                const InputTextField(
                    title: "Name of food", hint: "Enter name here..."),
                const SizedBox(height: 10),
                const InputTextField(
                    title: "Quantity", hint: "Enter quantity here..."),
                const SizedBox(height: 10),
                const Row(
                  children: [
                    Expanded(
                      child: InputTextField(
                          title: "Calories", hint: "Enter calories..."),
                    ),
                    SizedBox(width: 10),
                    Expanded(
                      child: InputTextField(
                          title: "Protein", hint: "Enter protein..."),
                    ),
                  ],
                ),
                const SizedBox(height: 10),
                const Row(
                  children: [
                    Expanded(
                      child: InputTextField(
                          title: "Fats (saturated)", hint: "Enter fats..."),
                    ),
                    SizedBox(width: 10),
                    Expanded(
                      child: InputTextField(
                          title: "Fats (unsaturated)", hint: "Enter fats..."),
                    ),
                  ],
                ),
              ],
            )),
            const SizedBox(height: 10),
            const SubmitButton(label: "Done"),
            const SizedBox(height: 10),
          ],
        ),
      ),
    );
  }
}
