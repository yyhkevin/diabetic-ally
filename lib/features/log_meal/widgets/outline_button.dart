import 'package:diabetic_ally/core/theme_colors.dart';
import 'package:flutter/material.dart';

class OutlineButton extends StatelessWidget {
  final String label;
  const OutlineButton({Key? key, required this.label}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      padding: const EdgeInsets.all(8),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8),
        border: Border.all(color: const Color(ThemeColors.prime)),
      ),
      child: Text(
        label,
        style: const TextStyle(color: Color(ThemeColors.prime)),
      ),
    );
  }
}
