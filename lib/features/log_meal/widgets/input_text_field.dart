import 'package:flutter/material.dart';

class InputTextField extends StatelessWidget {
  final String title;
  final String hint;

  const InputTextField({Key? key, required this.title, required this.hint})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          title,
          style: const TextStyle(fontWeight: FontWeight.bold),
        ),
        Container(
          decoration: BoxDecoration(
              border: Border.all(width: 0.5),
              borderRadius: BorderRadius.circular(8)),
          padding: const EdgeInsets.only(left: 8),
          child: TextField(
            maxLines: 1,
            decoration:
                InputDecoration(hintText: hint, border: InputBorder.none),
          ),
        ),
      ],
    );
  }
}
