import 'package:camera/camera.dart';
import 'package:diabetic_ally/core/theme_colors.dart';
import 'package:diabetic_ally/features/log_meal/widgets/submit_button.dart';
import 'package:flutter/material.dart';

class LogCameraPage extends StatefulWidget {
  const LogCameraPage({Key? key}) : super(key: key);

  @override
  State<LogCameraPage> createState() => _LogCameraPageState();
}

class _LogCameraPageState extends State<LogCameraPage> {
  late CameraController _controller;
  Future<void>? _initializeControllerFuture;

  @override
  void initState() {
    super.initState();

    setupCam();
  }

  void setupCam() async {
    // Obtain a list of the available cameras on the device.
    final cameras = await availableCameras();

    // Get a specific camera from the list of available cameras.
    final firstCamera = cameras.first;

    _controller = CameraController(
      // Get a specific camera from the list of available cameras.
      firstCamera,
      // Define the resolution to use.
      ResolutionPreset.medium,
    );

    // Next, initialize the controller. This returns a Future.
    _initializeControllerFuture = _controller.initialize();

    _initializeControllerFuture!.then((value) => setState(() {}));
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<void>(
      future: _initializeControllerFuture,
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          // If the Future is complete, display the preview.
          return Scaffold(
              body: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(child: CameraPreview(_controller)),
              const SizedBox(height: 10),
              Row(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  const SizedBox(width: 100),
                  GestureDetector(
                    onTap: () {
                      Navigator.of(context).pop();
                    },
                    child: const Text(
                      "Cancel",
                      style: TextStyle(color: Colors.red),
                    ),
                  ),
                  const SizedBox(width: 50),
                  GestureDetector(
                      onTap: () {
                        // TODO get picture and return to previous page
                        Navigator.of(context).pop();
                      },
                      child: const Icon(Icons.camera, size: 36)),
                ],
              ),
              const SizedBox(height: 10),
            ],
          ));
        } else {
          // Otherwise, display a loading indicator.
          return const Scaffold(
              body: Center(child: CircularProgressIndicator()));
        }
      },
    );
  }
}
