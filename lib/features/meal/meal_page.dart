import 'package:flutter/material.dart';

class MealPage extends StatelessWidget {
  const MealPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: SafeArea(
        child: ListView(
          padding: EdgeInsets.zero,
          children: [
            FittedBox(
              child: Image.asset(
                "assets/images/oat.png",
              ),
            ),
            const Padding(
              padding: EdgeInsets.all(16),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text("Breakfast",
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 18)),
                  SizedBox(height: 8),
                  Row(
                    children: [
                      Icon(Icons.schedule),
                      SizedBox(width: 5),
                      Text("09:30"),
                    ],
                  ),
                  SizedBox(height: 15),
                  Text("Recommended Meal",
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                      )),
                  Text("""- 1/2 cup quick-cook oats
- 1 banana

Boil 1 cup of hot water in a pot and cook 1/2 cups of oats in the boiling water for 3 minutes.
Cut a medium-sized banana into slices.
Serve the oats together with the banana.

Other Suitable Food Options:
1. Oatmeal
2. Greek yogurt with berries
3. Overnight chia seed pudding
4. Multigrain avocado toast
5. Cottage cheese, fruit, and nut bowl
6. Multigrain toast with nut butter
7. Tofu scramble with multigrain toast"""),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
