import 'package:diabetic_ally/core/advanced_list.dart';
import 'package:diabetic_ally/features/community/community_page.dart';
import 'package:diabetic_ally/features/community/widgets/community_post_widget.dart';
import 'package:diabetic_ally/features/learn/models/article_model.dart';
import 'package:diabetic_ally/features/learn/widgets/articles_section_widget.dart';
import 'package:diabetic_ally/features/learn/widgets/learn_page_section_header.dart';
import 'package:diabetic_ally/features/learn/widgets/videos_section_widget.dart';
import 'package:flutter/material.dart';

class LearnPage extends StatelessWidget {
  const LearnPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final List<ArticleModel> articles = [
      const ArticleModel(
        "What is diabetes?",
        "There are many different types of diabetes and today we look into Type 1, Type 2 and Gestational Diabetes. Type 1..",
        "assets/images/diabetes.jpg",
      ),
      const ArticleModel(
        "Nourishing Balance: 10 Natural Ways to Combat Diabetes",
        "Nutrition and physical activity are important parts of a healthy lifestyle when you have diabetes. Along with other..",
        "assets/images/foods.jpg",
      ),
    ];

    final List<String> videos = [
      "assets/images/vid_gestational.jpg",
      "assets/images/vid_t2.jpg",
    ];

    return SafeArea(
        child: SingleChildScrollView(
      padding: const EdgeInsets.symmetric(horizontal: 12),
      child: Column(
        children: [
          const SizedBox(height: 8),
          const LearnPageSectionHeader(
              iconData: Icons.article_outlined, label: "Articles"),
          const SizedBox(height: 10),
          SizedBox(
            height: 160,
            child: ArticlesSectionWidget(articles: articles),
          ),
          const SizedBox(height: 20),
          const LearnPageSectionHeader(
              iconData: Icons.video_camera_front_outlined, label: "Videos"),
          const SizedBox(height: 10),
          SizedBox(height: 160, child: VideosSectionWidget(videos: videos)),
          const SizedBox(height: 20),
          const LearnPageSectionHeader(
              iconData: Icons.local_library_outlined, label: "Community"),
          const SizedBox(height: 10),
          ...AdvancedList<Widget>([
            ...CommunityPage.posts.map((e) => CommunityPostWidget(post: e))
          ]).combineBetween(
            const Divider(height: 25, thickness: 0.5),
          ),
          const SizedBox(height: 10),
        ],
      ),
    ));
  }
}
