import 'package:flutter/material.dart';

class LearnPageSectionHeader extends StatelessWidget {
  final IconData iconData;
  final String label;
  const LearnPageSectionHeader(
      {Key? key, required this.iconData, required this.label})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Icon(iconData, size: 24),
        const SizedBox(width: 8),
        Text(
          label,
          style: const TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 20,
          ),
        ),
      ],
    );
  }
}
