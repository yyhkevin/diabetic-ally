import 'package:flutter/material.dart';

class VideosSectionWidget extends StatelessWidget {
  const VideosSectionWidget({
    super.key,
    required this.videos,
  });

  final List<String> videos;

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      scrollDirection: Axis.horizontal,
      itemCount: videos.length,
      itemBuilder: (context, index) {
        return Container(
          decoration: BoxDecoration(
            border: Border.all(width: 0.4),
            borderRadius: BorderRadius.circular(8),
          ),
          child: Stack(
            alignment: Alignment.center,
            children: [
              ClipRRect(
                borderRadius: BorderRadius.circular(8),
                child: ColorFiltered(
                  colorFilter: ColorFilter.mode(
                    Colors.white.withOpacity(0.3),
                    BlendMode.modulate,
                  ),
                  child: Image.asset(videos[index], fit: BoxFit.cover),
                ),
              ),
              Container(
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    border: Border.all(width: 0.5),
                  ),
                  height: 50,
                  width: 50,
                  child: const Icon(Icons.play_arrow)),
            ],
          ),
        );
      },
      separatorBuilder: (context, index) {
        return const SizedBox(width: 8);
      },
    );
  }
}
