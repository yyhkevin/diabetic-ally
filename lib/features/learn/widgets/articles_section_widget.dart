import 'package:diabetic_ally/features/learn/models/article_model.dart';
import 'package:flutter/material.dart';

class ArticlesSectionWidget extends StatelessWidget {
  const ArticlesSectionWidget({
    super.key,
    required this.articles,
  });

  final List<ArticleModel> articles;

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      scrollDirection: Axis.horizontal,
      itemCount: articles.length,
      itemBuilder: (context, index) {
        return Container(
          height: 150,
          width: 225,
          decoration: BoxDecoration(
            border: Border.all(width: 0.4),
            borderRadius: BorderRadius.circular(8),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ClipRRect(
                  borderRadius: const BorderRadius.only(
                    topLeft: Radius.circular(8),
                    topRight: Radius.circular(8),
                  ),
                  child: Image.asset(
                    articles[index].imageUri,
                    width: 225,
                    height: 100,
                    fit: BoxFit.cover,
                  )),
              Expanded(
                child: Container(
                  padding: const EdgeInsets.all(5),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(articles[index].title,
                          maxLines: 1,
                          style: const TextStyle(fontWeight: FontWeight.bold)),
                      Expanded(
                        child: Text(
                          articles[index].contents,
                          overflow: TextOverflow.ellipsis,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        );
      },
      separatorBuilder: (context, index) {
        return const SizedBox(width: 8);
      },
    );
  }
}
