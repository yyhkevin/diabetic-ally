class ArticleModel {
  final String title;
  final String contents;
  final String imageUri;

  const ArticleModel(this.title, this.contents, this.imageUri);
}
