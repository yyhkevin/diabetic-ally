import 'package:diabetic_ally/core/theme_colors.dart';
import 'package:diabetic_ally/features/community/community_page.dart';
import 'package:diabetic_ally/features/home/home_page.dart';
import 'package:diabetic_ally/features/learn/learn_page.dart';
import 'package:diabetic_ally/features/log_meal/log_meal_page.dart';
import 'package:diabetic_ally/features/profile/profile_page.dart';
import 'package:flutter/material.dart';

class LayoutPage extends StatefulWidget {
  const LayoutPage({Key? key}) : super(key: key);

  static const items = [
    {
      'icon': Icons.home,
      'label': "Home",
      'widget': HomePage(),
    },
    {
      'icon': Icons.restaurant,
      'label': "Log Meal",
      'widget': LogMealPage(),
    },
    {
      'icon': Icons.people,
      'label': "Community",
      'widget': CommunityPage(),
    },
    {
      'icon': Icons.school,
      'label': "Learn",
      'widget': LearnPage(),
    },
    {
      'icon': Icons.account_circle,
      'label': "Profile",
      'widget': ProfilePage(),
    },
  ];

  @override
  State<LayoutPage> createState() => _LayoutPageState();
}

class _LayoutPageState extends State<LayoutPage> {
  int selectedIndex = 0;

  @override
  Widget build(BuildContext context) {
    final mainWidget = LayoutPage.items[selectedIndex]['widget'] as Widget?;

    return Scaffold(
      body: Column(
        children: [
          Expanded(
            child: mainWidget ?? const Text("TODO"),
          ),
          Container(
            color: const Color(ThemeColors.prime),
            height: 60,
            child: Row(
              children: [
                for (int i = 0; i < LayoutPage.items.length; i++)
                  Expanded(
                    child: GestureDetector(
                      behavior: HitTestBehavior.opaque,
                      onTap: () {
                        setState(() {
                          selectedIndex = i;
                        });
                      },
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Icon(
                            LayoutPage.items[i]['icon'] as IconData,
                            color: Colors.white,
                            size: i == selectedIndex ? 28 : 24,
                          ),
                          Text(LayoutPage.items[i]['label'] as String,
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: i == selectedIndex
                                      ? FontWeight.bold
                                      : FontWeight.normal)),
                        ],
                      ),
                    ),
                  )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
