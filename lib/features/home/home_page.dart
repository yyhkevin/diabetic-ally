import 'package:diabetic_ally/core/theme_colors.dart';
import 'package:diabetic_ally/features/community/community_page.dart';
import 'package:diabetic_ally/features/home/widgets/steps_progress_indicator.dart';
import 'package:diabetic_ally/features/meal/meal_page.dart';
import 'package:diabetic_ally/features/medicine/medicine_page.dart';
import 'package:flutter/material.dart';
import 'package:semicircle_indicator/semicircle_indicator.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  String get greeting {
    final now = DateTime.now();
    if (now.hour < 12) {
      return "☀️ Good Morning";
    } else if (now.hour < 18) {
      return "☀️ Good Afternoon";
    } else {
      return "🌙 Good Evening";
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: ListView(
      padding: const EdgeInsets.all(16),
      children: [
        Center(
            child: Text(
          "$greeting, Tan Wei Jie!",
          style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
        )),
        const SizedBox(height: 30),
        const Center(
          child: SemicircularIndicator(
            color: Color(ThemeColors.prime),
            progress: 0.7,
            child: Text(
              "70",
              style: TextStyle(color: Color(ThemeColors.prime), fontSize: 36),
            ),
          ),
        ),
        const SizedBox(height: 30),
        const Padding(
          padding: EdgeInsets.only(left: 24.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text("Vitals",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18)),
              Row(
                children: [
                  Row(
                    children: [
                      Icon(Icons.favorite, color: Colors.red),
                      SizedBox(width: 5),
                      Text("76", style: TextStyle(fontWeight: FontWeight.bold))
                    ],
                  ),
                  SizedBox(width: 40),
                  Row(
                    children: [
                      Icon(Icons.bloodtype, color: Colors.red),
                      SizedBox(width: 5),
                      Text("4.0 mmol/L",
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.green)),
                    ],
                  ),
                  SizedBox(width: 40),
                  Row(
                    children: [
                      Icon(
                        Icons.sports,
                      ),
                      SizedBox(width: 5),
                      Text("1050",
                          style: TextStyle(fontWeight: FontWeight.bold)),
                    ],
                  ),
                ],
              ),
            ],
          ),
        ),
        const SizedBox(height: 30),
        const Text("Today",
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16)),
        const SizedBox(height: 10),
        StepsProgressWidget(
          steps: const [
            "09:15\nTake Medication X\n2 Pills",
            "09:30\nBreakfast\n1/2 cup quick-cook oats, 1 banana",
            "10:30\nBrisk Walking\n30 minutes exercise",
            "12:30\nLunch\nWhite chicken rice without skin",
          ],
          onTap: [
            () {
              Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) => const MedicinePage()));
            },
            () {
              Navigator.of(context).push(
                  MaterialPageRoute(builder: (context) => const MealPage()));
            },
            () {
              //TODO
            },
            () {
              //TODO
            },
          ],
          currentStep: 0,
          undoneCircleColor: Colors.black,
          doneCircleColor: Colors.black,
        ),
      ],
    ));
  }
}
