import "package:flutter/material.dart";

class StepsProgressWidget extends StatelessWidget {
  final List<String> steps;
  final List<Function> onTap;
  final int currentStep;
  final Color? undoneCircleColor;
  final Color? undoneLineColor;
  final Color? doneCircleColor;
  final Color? doneLineColor;

  const StepsProgressWidget({
    super.key,
    required this.steps,
    required this.onTap,
    required this.currentStep,
    this.undoneCircleColor,
    this.undoneLineColor,
    this.doneCircleColor,
    this.doneLineColor,
  })  : assert(steps.length > 0),
        assert(currentStep < steps.length + 1);

  @override
  Widget build(BuildContext context) {
    final undoneCircleColor = this.undoneCircleColor ?? Colors.grey[300];
    final undoneLineColor = this.undoneLineColor ?? undoneCircleColor;
    final doneCircleColor = this.doneCircleColor ?? Colors.lightGreen;
    final doneLineColor =
        this.doneLineColor ?? doneCircleColor.withOpacity(0.5);

    return Column(
      children: [
        for (int i = 0; i < steps.length; i++)
          IntrinsicHeight(
            child: Stack(
              children: [
                if (i < steps.length - 1)
                  Container(
                    margin: const EdgeInsets.only(left: 5.5),
                    width: 1,
                    color: currentStep > i ? doneLineColor : undoneLineColor,
                  ),
                Column(
                  children: [
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: currentStep > i
                                ? doneCircleColor
                                : undoneCircleColor,
                          ),
                          height: 12,
                          width: 12,
                        ),
                        const SizedBox(width: 8),
                        Expanded(
                          child: GestureDetector(
                            onTap: () => onTap[i](),
                            behavior: HitTestBehavior.opaque,
                            child: Container(
                              decoration: BoxDecoration(
                                  border: Border.all(width: 0.3),
                                  borderRadius: BorderRadius.circular(8)),
                              padding: const EdgeInsets.all(5),
                              child: Text(
                                steps[i],
                                style: const TextStyle(
                                  fontSize: 14,
                                  fontWeight: FontWeight.w400,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    if (i < steps.length - 1) const SizedBox(height: 16),
                  ],
                ),
              ],
            ),
          ),
      ],
    );
  }
}
