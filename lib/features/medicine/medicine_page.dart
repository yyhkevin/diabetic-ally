import 'package:diabetic_ally/features/log_meal/widgets/outline_button.dart';
import 'package:diabetic_ally/features/log_meal/widgets/submit_button.dart';
import 'package:flutter/material.dart';

class MedicinePage extends StatelessWidget {
  const MedicinePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: [
            Expanded(
              child: ListView(
                children: [
                  SizedBox(
                      height: 180,
                      child: Image.asset("assets/images/medicineX.png",
                          fit: BoxFit.cover)),
                  Padding(
                    padding: const EdgeInsets.all(12),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text("Medication X",
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 18)),
                        const SizedBox(height: 8),
                        const Row(
                          children: [
                            Icon(Icons.schedule),
                            SizedBox(width: 5),
                            Text("09:15"),
                          ],
                        ),
                        const SizedBox(height: 8),
                        const Row(
                          children: [
                            Icon(Icons.medication_outlined),
                            SizedBox(width: 5),
                            Text("2 pills"),
                          ],
                        ),
                        const SizedBox(height: 15),
                        const Text("Frequency - Twice a day",
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                            )),
                        const Text("""- 09:15 (2 pills)\n- 14:00 (2 pills)"""),
                        const SizedBox(height: 15),
                        const Text("Directions of use",
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                            )),
                        const Text(
                            "Take two pills with a full glass of water as needed. Do not exceed the recommended dosage."),
                        const SizedBox(height: 15),
                        const Text("What is Medication X?",
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                            )),
                        const Text(
                            "Medication X stabilizes blood sugar levels. They enhance insulin sensitivity and provide a sustained energy boost, combating fatigue associated with diabetes."),
                        const SizedBox(height: 10),
                        Row(
                          children: [
                            Stack(
                              alignment: Alignment.center,
                              children: [
                                SizedBox(
                                  height: 70,
                                  width: 105,
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(8),
                                    child: ColorFiltered(
                                      colorFilter: ColorFilter.mode(
                                        Colors.white.withOpacity(0.3),
                                        BlendMode.modulate,
                                      ),
                                      child: Image.asset(
                                          "assets/images/medicineX.png",
                                          fit: BoxFit.cover),
                                    ),
                                  ),
                                ),
                                Container(
                                    decoration: BoxDecoration(
                                      shape: BoxShape.circle,
                                      border: Border.all(width: 0.5),
                                    ),
                                    height: 30,
                                    width: 30,
                                    child: const Icon(Icons.play_arrow)),
                              ],
                            ),
                            const SizedBox(width: 8),
                            const Text("About Medication X and its dosage"),
                          ],
                        ),
                        const SizedBox(height: 10),
                        GestureDetector(
                          onTap: () {
                            showDialog(
                              context: context,
                              builder: (context) => const AlertDialog(
                                title: Text(
                                  "Side effects of Medication X",
                                  style: TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.bold),
                                ),
                                content: Text("""
- Nausea
- Dizziness
- Stomachache
- Bloating
- Diarrhea
- Dehydration
- Loss of appetite

These symptoms are common and normal. If you are in doubt, please consult your physician on your condition."""),
                              ),
                            );
                          },
                          child: const Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Icon(Icons.warning, color: Colors.red),
                              SizedBox(width: 5),
                              Text(
                                  "Click here to know more about the side effects.",
                                  style: TextStyle(color: Colors.red)),
                            ],
                          ),
                        ),
                        const SizedBox(height: 10),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            const SizedBox(height: 10),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                GestureDetector(
                    onTap: () {
                      Navigator.of(context).pop();
                    },
                    child: const OutlineButton(label: "Return to Home")),
                const SizedBox(width: 10),
                const SubmitButton(label: "I have taken"),
              ],
            ),
            const SizedBox(height: 10),
          ],
        ),
      ),
    );
  }
}
