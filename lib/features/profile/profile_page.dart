import 'package:diabetic_ally/core/advanced_list.dart';
import 'package:diabetic_ally/core/theme_colors.dart';
import 'package:diabetic_ally/features/profile/widgets/setting_row.dart';
import 'package:flutter/material.dart';

class ProfilePage extends StatelessWidget {
  const ProfilePage({Key? key}) : super(key: key);

  static const settings = [
    "Profile",
    "Emergency Contacts",
    "Settings",
    "About",
  ];

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: ListView(
      padding: const EdgeInsets.all(55),
      children: [
        const Center(
            child: Icon(
          Icons.account_circle,
          size: 64,
          color: Color(ThemeColors.prime),
        )),
        const Center(
          child: Text(
            "Tan Wei Jie",
            style: TextStyle(
              fontSize: 32,
              color: Color(ThemeColors.prime),
            ),
          ),
        ),
        const SizedBox(height: 30),
        ...AdvancedList(List.from(settings.map((s) => SettingRow(label: s))))
            .combineBetween(const SizedBox(height: 10)),
      ],
    ));
  }
}
