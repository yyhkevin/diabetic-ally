import 'package:flutter/material.dart';

class SettingRow extends StatelessWidget {
  final String label;

  const SettingRow({Key? key, required this.label}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
            child: Text(
          label,
          style: const TextStyle(fontSize: 18),
        )),
        const Icon(Icons.chevron_right),
      ],
    );
  }
}
