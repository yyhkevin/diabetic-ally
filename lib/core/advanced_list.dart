class AdvancedList<T> {
  final List<T> mainList;

  AdvancedList(this.mainList);

  List<T> combineBetween(T add) {
    if (mainList.isEmpty) return mainList;
    final output = <T>[];

    for (final item in mainList) {
      output.add(item);
      output.add(add);
    }
    output.removeLast();

    return output;
  }
}
